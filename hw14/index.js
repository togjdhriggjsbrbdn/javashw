let tabs = $(".tabs-title");
let content = $(".tabs-content");



tabs.not(".active").on("click", function () {
    let index = $(this).index();
    $(this).addClass("active").siblings().removeClass("active");
    content.removeClass("active").eq(index).addClass("active");
});

$(tabs.eq(0)).addClass('active');
$(content.eq(0)).addClass('active');
