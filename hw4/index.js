function createNewUser() {
    let firstName = prompt("Please enter your first name");
    while(parseInt(firstName) || !firstName.trim() || !firstName) firstName = prompt("Please enter a valid first name, you wrote: " + firstName);

    let lastName = prompt("Please enter your last name");
    while(parseInt(lastName) || !lastName.trim() || !lastName) lastName = prompt("Please enter a valid last name, you wrote: " + lastName);

    return {
        _firstName: firstName,
        _lastName: lastName,
        get firstName() {
            return this._firstName
        },
        get lastName() {
            return this._lastName
        },
        set setFirstName(value) {
            this._firstName = value
        },
        set setLastName(value) {
            this._lastName = value
        },
        getLogin: function () {
            return this.firstName.charAt(0).toLowerCase() + this.lastName
        }
    };
}

const newUser = createNewUser();
console.log(newUser.getLogin());

newUser.setFirstName = "Ivan";
newUser.setLastName = "Kravchenko";
console.log(newUser.getLogin());

