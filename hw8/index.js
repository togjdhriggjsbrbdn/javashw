let input = document.getElementById("main-input");
let mainBlock = document.getElementById("main-block");


let currentPrice = document.createElement("div");
mainBlock.insertBefore(currentPrice, mainBlock.children[0]);
currentPrice.classList.add("current-price");
currentPrice.style.cssText = "display: none";

let resetCorrectSpan = document.createElement("span");
resetCorrectSpan.classList.add("reset-span");
resetCorrectSpan.style.cssText = "display: none";
resetCorrectSpan.innerHTML = "x";

let errorSpan = document.createElement("span");
errorSpan.classList.add("error-span");
errorSpan.innerHTML = "Please enter correct price";
mainBlock.appendChild(errorSpan);
errorSpan.style.cssText = "display: none";



input.onblur = () => {
    input.style.cssText = "background-color: greenyellow;";
    let inputPrice = input.value;
    currentPrice.innerHTML = `Текущая цена ${inputPrice}`;
    if (inputPrice > -1) {
        currentPrice.style.cssText = "display: inline-block";
        resetCorrectSpan.style.cssText = "display: inline-block";
        errorSpan.style.cssText = "display: none";
        currentPrice.insertBefore(resetCorrectSpan, null);

        resetCorrectSpan.onclick = function () {
            currentPrice.style.cssText = "display: none;";
            resetCorrectSpan.style.cssText = "display: none;";
            input.style.cssText = "background-color: white;";
            input.value = "";
        }
    } else {
        input.style.cssText = "border: 1px solid green;";
        errorSpan.style.cssText = "display: inline-block";
        currentPrice.style.cssText = "display: none";
    }

};
