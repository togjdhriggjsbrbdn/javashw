let ArrOfImg = Array.from(document.getElementsByClassName("image-to-show"));
let currentImg = ArrOfImg[0];
let indexOfCurrentImg = ArrOfImg.indexOf(currentImg);
currentImg.classList.add("active");
let stopButton = document.getElementById("stop");
let startButton = document.getElementById("start");


function nextImg() {
    ArrOfImg[indexOfCurrentImg].classList.remove("active");
    indexOfCurrentImg++;
    if (indexOfCurrentImg === 4) {
        indexOfCurrentImg = 0;
    }
    ArrOfImg[indexOfCurrentImg].classList.add("active");
}

let interval = setInterval(nextImg, 10000);

stopButton.onclick = function () {
    clearInterval(interval);
    startButton.dataset.check = "1";
};

startButton.onclick = function () {
    if (startButton.dataset.check==="1") {
        console.log(stopButton.onclick);
        interval = setInterval(nextImg, 1000);
        startButton.dataset.check = "2";
    }
};
