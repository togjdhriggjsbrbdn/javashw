let Collection = document.getElementsByTagName("button");
let ArrayButtons = Array.from(Collection);


document.onkeydown = function (e) {
    ArrayButtons.forEach(function (elem) {
        if ( `Key${elem.innerHTML}` === e.code) {
            caseInner(elem.innerHTML)
        } else if ("Enter" === e.code) {
            caseInner("Enter")
        }
    });
};

function caseInner(symbol) {
    removeActive();
    document.getElementById(`${symbol}`).classList.add("active");

}

function removeActive() {
    ArrayButtons.forEach(function (elem) {
        elem.classList.remove("active");
    })
}

