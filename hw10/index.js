let visibilityIcon =  document.getElementById("visibility-icon");
let mainInput = document.getElementById("main-input");

let confirmationIcon = document.getElementById("confirmation-icon");
let confirmationInput = document.getElementById("confirmation-input");

let button = document.getElementById("main-button");

visibilityIcon.onclick = function () {
    showPass(visibilityIcon, mainInput);
};

confirmationIcon.onclick =function () {
    showPass(confirmationIcon, confirmationInput);
};

button.onclick = function () {
    if (mainInput.value === confirmationInput.value) {
        alert("You are welcome");
    } else {
        alert("Нужно ввести одинаковые значения");
    }
};

function showPass(icon, input) {
    if (icon.classList.contains("fa-eye")) {
        icon.classList.remove("fa-eye");
        icon.classList.add("fa-eye-slash");
        input.type = "password";
    } else {
        icon.classList.remove("fa-eye-slash");
        icon.classList.add("fa-eye");
        input.type = "text";
    }
}
