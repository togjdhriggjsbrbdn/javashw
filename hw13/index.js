let ArrGrayElements = Array.from(document.getElementsByClassName("bg-gray"));
let themeButton = document.getElementById("change");
ArrGrayElements.forEach(function (elem) {
    elem.style.backgroundColor = localStorage.getItem("color");
});
themeButton.setAttribute("data-state", localStorage.getItem("state"));

themeButton.onclick = function () {
    if (themeButton.getAttribute("data-state") === "1") {
        localStorage.setItem("color", "red");
        ArrGrayElements.forEach(function (elem) {
            elem.style.backgroundColor = localStorage.getItem("color");
        });
        themeButton.setAttribute("data-state", "2");
        localStorage.setItem("state", "2");
    } else {
        localStorage.setItem("color", "#f2f2f2");
        ArrGrayElements.forEach(function (elem) {
            elem.style.backgroundColor = localStorage.getItem("color");
        });
        themeButton.setAttribute("data-state", "1");
        localStorage.setItem("state", "1");
    }
};




