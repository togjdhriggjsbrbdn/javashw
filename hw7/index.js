function createList(arr){
    let df = document.createDocumentFragment();
    let ul = document.createElement("ul");
    arr.map(function(element){
        let li = document.createElement("li");
        if(Array.isArray(element)){
            let childUl = document.createElement("ul");
            element.map(function(subElement){
                let childLi = document.createElement("li");
                childLi.innerHTML = `<a href='#' title='${subElement}'>${subElement}</a>`;
                childUl.appendChild(childLi);
            });
            li.appendChild(childUl);
        }
        else{
            li.innerHTML =  `<a href='#' title='${element}'>${element}</a>`;
        }
        ul.appendChild(li);

    });
    df.appendChild(ul);
    document.body.appendChild(df);
}

createList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', 1, 2 , 3, 4,]);
