function filterBy(arr,filter){
    const filteredArr = [];
    arr.forEach(function(element){
        if(filter === 'null'){
            if(element)
                filteredArr.push(element);
        }
        else if(filter === 'object'){
            if(element === null || typeof element !== filter)
                filteredArr.push(element);

        }
        else{
            if(typeof element !== filter){
                filteredArr.push(element);
            }

        }
    });
    return filteredArr;
}
const arr = ['hello', 'world', 23, '23', null];
console.log("Array to filter: " + arr);
console.log("Filtering by strings");
console.log(filterBy(arr,'string'));
console.log("Filtering by number");
console.log(filterBy(arr,'number'));
console.log("Filtering by boolean");
console.log(filterBy(arr,'boolean'));

console.log("Filtering by object");
console.log(filterBy(arr,'object'));


console.log("Filtering by null");
console.log(filterBy(arr,'null'));

